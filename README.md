# A helper script to generate RevBayes scripts for biogeographic (DEC) analyses

## Table of content


[TOC]


## Repository organization

* The **Scripts** folder contains the _defineAnalysis.py_ that generates the Rev Scripts.
* The **Dataset** folder contains an example dataset (_Hypostomus_)

## Using the helper script

### Requirements
* Python2.7
* python-nexus (Install with _pip install python-nexus_)
* RevBayes

### Usage

> python Scripts/defineAnalysis.py runType matrixType rangesFile epochsFile connectivityFile dataFile genScriptFolder

The arguments are:

* **runType** is the type of analyses:
    * **0**: Validation-mode: constant parameters with one epoch.
    * **1**: Single epoch mode (one DEC matrix)
    * **2**: Multiple epochs mode (_K_ DEC matrices - one per epoch)
* **matrixType** is the type of cladogenetic matrix
    * **0**: Allopatry and subset sympatry rates are constant
    * **1**: Allopatry and subset sympatry rates are estimated and are constant through time (one cladogenetic matrix)
    * **2**: Allopatry and subset sympatry rates are estimated per epochs ( _K_ cladogenetic matrices)
* **rangesFile** is the file specifying the areas and ranges to include in the analyses    
    * Areas ordering must match the one used in the **dataFile**
    * See "_Dataset/Example/ranges.txt_" for an example
* **epochsFile** is the file containing the timing of epochs
    * See "_Dataset/Example/epochsTimes.txt_" for an example with two epoch boundaries (i.e., one estimated between 0 and 7, and one estimated between time 7 and 15)
* **connectivityFile** is the file(s) containing the connectivity matrices per epoch
    * When **X** is set to **0** or **1** this variable must point a single file  (e.g., "_hyp.connectivity.1.txt_")
    * When **X** is set to **2** this variable must identify the _basename_ of the file (e.g., "_hyp.connectivity._" for files "_hyp.connectivity.**I**.txt_" where **I** is the epoch index)
    * See "_Dataset/Example/hyp.connectivity.1.txt_" or "_Dataset/Example/hyp.connectivity.2.txt_" for an example
* **dataFile** is the original nexus file containing the observed ranges
* **genScriptFolder** is the folder where the generated RevBayes scripts will be written (must be a valid/existing path)

### Advanced usage
The type of cladogenetic events considered in the analysis can be specified by changing the *eventTypes* variable in the _defineAnalysis.py_ script (line 780). By default allopatric and subset sympatric events are considered (i.e., 'a' and 's' events in RevBayes terminology). The list is therefore set to:

```python
         eventTypes = ['a', 's']
```

The event list can be augmented with narrow sympatric events ('n') or full sympatric events ('f'), e.g.,:

```python
        eventTypes = ['a', 's', 'f']
```

### Outcome
Executing the _defineAnalysis.py_ script will generate 5 files in the **genScriptFolder**:

* **adaptedData.tsv**
    * Contains the observed ranges encoded to fit the generated model
* **generated_Init.Rev**
    * Initialize the parameters and constants
* **generated_QMatrices.Rev**
    * Initialize the Q_DEC rates matrices
* **generated_CladoProb.Rev**
    * Initialize the cladogenetic probability lists ('s' and 'a' events)
* **generated_Model.Rev**
    * Initialize the model and the matrices objects

### Launching an analysis

1. The variable pointing to the tree file (_newick file_) must be manually initialized and the **relPath** must be set to the relative or absolute path leading to the **genScriptFolder**
    * For instance, if files are generated in _/home/user/biogeo/generatedScripts/_ then **relPath**=_/home/user/biogeo_ or _._ if the analysis script is called from _/home/user/biogeo_).
2. The four generated Rev scripts must be __sourced__ in order to create the model and the moves.
3. The RevBayes _moves_ variable (i.e., array containing the MCMC proposals) defined in **generated_Init.Rev** and the model defined in **generated_Model.Rev** can then be used for your analyses (MCMC, power posterior, etc.).

See the file __Dataset/Hypostomus/runAnalysisM1.Rev__ for an example.


## Tutorials for the _Hypostomus_ dataset

These tutorials assume that you execute the commands at the _root_ of the repository (i.e., folder containing the _Dataset_ and _Scripts_) and that you are in a linux or mac terminal.

### Case 1: One epoch, full connectivity (Hypothesis M1)

* Defining where the generated script will be placed
```bash
        me@terminal:~$ GEN_SCRIPT_FOLDER="./GeneratedScripts/MyScriptsM1"
```

* Creating the folder in question
```bash
        me@terminal:~$ mkdir -p ${GEN_SCRIPT_FOLDER}
```

* Defining where to find the data
```bash
        me@terminal:~$ DATA_FOLDER="./Dataset/Hypostomus/HypothesisM1"
```

* Creating the RevBayes scripts for 1 epoch
```bash
        me@terminal:~$ python Scripts/defineAnalysis.py 1 1 \
                       ${DATA_FOLDER}/ranges.txt \
                       ${DATA_FOLDER}/hyp.times.txt \
                       ${DATA_FOLDER}/hyp.connectivity.1.txt \
                       ${DATA_FOLDER}/hyp.range.nex \
                       ${GEN_SCRIPT_FOLDER}
```

* The __Dataset/Hypostomus/runAnalysisM1.Rev__ can then be used as a basis to run your analysis.
```bash
        me@terminal:~$ rb Dataset/Hypostomus/runAnalysisM1.Rev
```

### Case 2: One epoch, reduced connectivity (Hypothesis M2)

This case is mostly the same than the previous one (Case 1) but with different path for the _GEN_SCRIPT_FOLDER_ and _DATA_FOLDER_.

* Defining where the generated script will be placed
```bash
        me@terminal:~$ GEN_SCRIPT_FOLDER="./GeneratedScripts/MyScriptsM2"
```

* Creating the folder in question
```bash
        me@terminal:~$ mkdir -p ${GEN_SCRIPT_FOLDER}
```

* Defining where to find the data
```bash
        me@terminal:~$ DATA_FOLDER="./Dataset/Hypostomus/HypothesisM2"
```

* Creating the RevBayes scripts for 1 epoch
```bash
        me@terminal:~$ python Scripts/defineAnalysis.py 1 1 \
                       ${DATA_FOLDER}/ranges.txt \
                       ${DATA_FOLDER}/hyp.times.txt \
                       ${DATA_FOLDER}/hyp.connectivity.1.txt \
                       ${DATA_FOLDER}/hyp.range.nex \
                       ${GEN_SCRIPT_FOLDER}
```

* The __Dataset/Hypostomus/runAnalysisM2.Rev__ can then be used as a basis to run your analysis.
```bash
        me@terminal:~$ rb Dataset/Hypostomus/runAnalysisM2.Rev
```

### Case 3: Multiple epochs, reduced connectivity (Hypothesis M3)

This case is mostly the same than the previous cases but:
* with different path for the _GEN_SCRIPT_FOLDER_ and _DATA_FOLDER_
* with different settings to reflect that we are specifying multiple epochs, i.e.:
    * **runType=2**
    * **connectivityFile=${DATA_FOLDER}/hyp.connectivity.**

* Defining where the generated script will be placed
```bash
        me@terminal:~$ GEN_SCRIPT_FOLDER="./GeneratedScripts/MyScriptsM3"
```

* Creating the folder in question
```bash
        me@terminal:~$ mkdir -p ${GEN_SCRIPT_FOLDER}
```

* Defining where to find the data
```bash
        me@terminal:~$ DATA_FOLDER="./Dataset/Hypostomus/HypothesisM3"
```

* Creating the RevBayes scripts for multiple epochs
```bash
        me@terminal:~$ python Scripts/defineAnalysis.py 2 1 \
                       ${DATA_FOLDER}/ranges.txt \
                       ${DATA_FOLDER}/hyp.times.txt \
                       ${DATA_FOLDER}/hyp.connectivity. \
                       ${DATA_FOLDER}/hyp.range.nex \
                       ${GEN_SCRIPT_FOLDER}
```

* The __Dataset/Hypostomus/runAnalysisM3.Rev__ can then be used as a basis to run your analysis.
```bash
        me@terminal:~$ rb Dataset/Hypostomus/runAnalysisM3.Rev
```