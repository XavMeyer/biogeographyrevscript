#!/usb/bin/python

import sys
from os import path
from nexus import NexusReader

UGLY_MODE = True

typeOfRun = ['debugLik', 'singleEpoch', 'multiEpoch']
typeOfCladogeneticMatrix = ['constant', 'singleEpoch', 'multipleEpochs']

def isfloat(value):
    try:
        float(value)
        return True
    except ValueError:
        return False

def readRanges(rangesFile):

    inFile = open(rangesFile, 'r')
    areas = []
    ranges = []

    content = inFile.read()
    content = content.replace('\r\n', '\n');
    content = content.replace('\r', '\n');
    #print newContent
    lines = content.split('\n')
    #print lines

    areas.append('empty')
    ranges.append(['empty'])

    # Read the list of areas/states (ordered)
    line = lines[0] # inFile.readline()
    line = lines[1] # inFile.readline() # x,y,z
    words = line.split(',')
    areas += [w.strip() for w in words]

    # Read the list of possible ranges
    line = lines[2] #inFile.readline() # Possible ranges::
    for line in lines[3:]:
        if line == '' : continue
        #print line
        words = line.split(',')
        r = [w.strip() for w in words]
        r.sort()
        ranges.append(r)

    return [areas, ranges]

def readConnectivity(connectivityFileName):

    conMatrices = []
    coefficientsQ = set()

    inFile = open(connectivityFileName, 'r')

    content = inFile.read()
    content = content.replace('\r\n', '\n');
    content = content.replace('\r', '\n');
    #print newContent
    lines = content.split('\n')

    cMat = []
    cMat.append([])

    for line in lines:
        if line == '' : continue
        words = line.split()
        row = [1.]
        row += [w for w in words]

        for iR in range(len(row)):
            if not isfloat(row[iR]): coefficientsQ.add(row[iR])
            else: row[iR] = float(row[iR])

        cMat.append(row)

    cMat[0] = [1.0 for x in range(len(cMat[-1]))]
    conMatrices.append(cMat)

    return [conMatrices, list(coefficientsQ)]

def readConnectivities(connectivity_base_Name):

    conMatrices = []
    coefficientsQ = set()

    i = 1
    while path.exists(connectivity_base_Name + repr(i) + '.txt'):
        inFile = open(connectivity_base_Name + repr(i) + '.txt', 'r')
        cMat = []
        cMat.append([])

        content = inFile.read()
        content = content.replace('\r\n', '\n');
        content = content.replace('\r', '\n');
        #print newContent
        lines = content.split('\n')

        for line in lines:
            if line == '' : continue
            words = line.split()
            row = [1.]
            row += [w for w in words]

            for iR in range(len(row)):
                if not isfloat(row[iR]): coefficientsQ.add(row[iR])
                else: row[iR] = float(row[iR])

            cMat.append(row)

        cMat[0] = [1.0 for x in range(len(cMat[-1]))]
        #print cMat
        conMatrices.append(cMat)

        i += 1

    return [conMatrices, list(coefficientsQ)]

def readEpochs(epochsFile):
    epochs = []
    inFile = open(epochsFile, 'r')

    content = inFile.read()
    content = content.replace('\r\n', '\n');
    content = content.replace('\r', '\n');
    #print newContent
    lines = content.split('\n')

    for line in lines:
        if line == '' : continue
        words = line.split()
        epoch = [float(w) for w in words]
        epochs.append(epoch)
    return epochs


def buildBinaryAreasMap(areas):
    mapAreasToId = {}
    mapBinToArea = {}
    for i, area in zip(range(len(areas)), areas):
        mapAreasToId[area] = i
        bin = 0b1 << i
        mapBinToArea[bin] = area

    return [mapAreasToId, mapBinToArea]

def buildRangesMap(ranges):
    mapRangesToId = {}
    for i, r in zip(range(len(ranges)), ranges):
        mapRangesToId[tuple(r)] = i

    return mapRangesToId


def areaToBinary(area, mapAreas):
    return 0b1 << mapAreas[area]


def rangeToBinary(range, mapAreas):
    rangeBin = 0b0;
    for area in range:
        areaBin = areaToBinary(area, mapAreas)
        rangeBin = rangeBin | areaBin;

    return rangeBin

def buildQMatrix(iEpoch, areas, ranges, conMatrix, coefficientsQ, includeEmptyState):

    iOffset = 0

    [mapAreasToId, mapBinToArea] = buildBinaryAreasMap(areas)

    qMatrix = [['0.' for x in range(len(ranges))] for y in range(len(ranges))]
    for i in range(len(ranges)):
        for j in range(len(ranges)):

            fromRange = ranges[i]
            fromRangeBin = rangeToBinary(fromRange, mapAreasToId)

            toRange = ranges[j]
            toRangeBin = rangeToBinary(toRange, mapAreasToId)

            if j > i: # dispersal rates
                diff = toRangeBin ^ fromRangeBin
                nBitDiff = bin(diff).count("1")
                if nBitDiff == 1:
                    toArea = mapBinToArea[diff]
                    strDispersal = ''
                    for fromArea in fromRange:
                        fromAreaID = mapAreasToId[fromArea]
                        toAreaID = mapAreasToId[toArea]
                        #print fromAreaID, '  ', toAreaID
                        if len(coefficientsQ) == 0:
                            if len(strDispersal) > 0: strDispersal += '+'
                            strDispersal += repr(conMatrix[fromAreaID][toAreaID]) + '*D[' + repr(iEpoch) + '][' + repr(iOffset+fromAreaID) + '][' + repr(iOffset+toAreaID) +']'
                        else:
                            if not isfloat(conMatrix[fromAreaID][toAreaID]):
                                if len(strDispersal) > 0: strDispersal += '+'
                                strDispersal += 'D[' + repr(iEpoch) + '][' + repr(iOffset+fromAreaID) + '][' + repr(iOffset+toAreaID) +']'
                            elif conMatrix[fromAreaID][toAreaID] > 0.:
                                if len(strDispersal) > 0: strDispersal += '+'
                                strDispersal += repr(conMatrix[fromAreaID][toAreaID])

                    if len(strDispersal) > 0: qMatrix[i][j] = strDispersal
            elif j < i: # extirpation one rate for all
                diff = toRangeBin ^ fromRangeBin
                nBitDiff = bin(diff).count("1")
                if includeEmptyState and toRange[0] == 'empty' and nBitDiff == 2:
                    lostArea = mapBinToArea[fromRangeBin]
                    lostAreaID = mapAreasToId[lostArea]
                    qMatrix[i][j] = 'E[' + repr(iEpoch) + '][' + repr(iOffset+lostAreaID) + ']'
                if nBitDiff == 1:
                    lostArea = mapBinToArea[diff]
                    lostAreaID = mapAreasToId[lostArea]
                    qMatrix[i][j] = 'E[' + repr(iEpoch) + '][' + repr(iOffset+lostAreaID) + ']'
    return qMatrix

def recursiveSubRanges(leftChildRange, rightChildRange, subRanges, limitedSubRanges):

    for iR, lR in zip(range(len(leftChildRange)), leftChildRange):
        newLeftChildRange = leftChildRange[:]
        del newLeftChildRange[iR]
        newRightChildRange = rightChildRange[:]
        newRightChildRange.append(lR)

        if len(newLeftChildRange) >= 1 and len(newRightChildRange) >= 1:
            newLeftChildRange.sort()
            newRightChildRange.sort()
            if not limitedSubRanges:
                subRange = (tuple(newLeftChildRange), tuple(newRightChildRange))
                subRanges.add(subRange)
            elif len(newLeftChildRange) == 1 or len(newRightChildRange) == 1 :
                subRange = (tuple(newLeftChildRange), tuple(newRightChildRange))
                subRanges.add(subRange)
            # else: nothing

        if len(newLeftChildRange) > 1:
            recursiveSubRanges(newLeftChildRange, newRightChildRange, subRanges, limitedSubRanges)

def buildAllopatricSubRanges(aRange, limitedSubRanges):
    subRanges = set()
    lRange = aRange[:]
    rRange = []
    recursiveSubRanges(lRange, rRange, subRanges, limitedSubRanges)

    return subRanges


def addOutcome(outcomes, descendants, type):
    if descendants in outcomes:
        outcomes[descendants] += [type]
    else:
        outcomes[descendants] = [type]

def addComment(comments, descendants, type, iR, ranges):
    if not descendants in comments:
        comments[descendants] = ' # '

    if type == 'n':
        comments[descendants] += '[ Narrow S. : ' + ','.join(ranges[iR]) + ' -> ' + ','.join(ranges[descendants[0]]) + ' | ' + ','.join(ranges[descendants[1]]) + ' ] '
    elif type == 'f':
        comments[descendants] += '[ Full S. : ' + ','.join(ranges[iR]) + ' -> ' + ','.join(ranges[descendants[0]]) + ' | ' + ','.join(ranges[descendants[1]]) + ' ] '
    elif type == 'a':
        comments[descendants] += '[ Allopatry : ' + ','.join(ranges[iR]) + ' -> ' + ','.join(ranges[descendants[0]]) + ' | ' + ','.join(ranges[descendants[1]]) + ' ] '
    elif type == 's':
        comments[descendants] += '[ Subset S. : ' + ','.join(ranges[iR]) + ' -> ' + ','.join(ranges[descendants[0]]) + ' | ' + ','.join(ranges[descendants[1]]) + ' ] '

def getPMatrixNormTerms(eventTypes, cladogeneticEventList):

    normTerms = []

    for iA in range(len(cladogeneticEventList)):
        outcomes = cladogeneticEventList[iA]
        if len(outcomes) <= 1:
            normTerms.append(None)
            continue # Empty or narrow sympatry

        allEvents = []
        for key, val in outcomes.iteritems():
            allEvents += val

        normTerm = []
        for eType in eventTypes:
            normTerm.append(allEvents.count(eType))

        normTerms.append(normTerm)

    return normTerms

def buildCladogeneticEvents(eventTypes, areas, ranges, conMatrix):

    [mapAreasToId, mapBinToArea] = buildBinaryAreasMap(areas)
    mapRangesToId = buildRangesMap(ranges)

    iStart = 1

    cladogeneticEventList = []
    cladogeneticCommentList = []

    for iR, ancestralRange in zip(range(iStart, len(ranges)), ranges[iStart:]):
        outcomes = {}
        comments = {}

        if len(ancestralRange) == 1: # narrow sympatry or 'n'
            addOutcome(outcomes, (iR, iR), 'n')
            addComment(comments, (iR, iR), 'n', iR, ranges)
            cladogeneticEventList.append(outcomes)
            cladogeneticCommentList.append(comments)
            continue

        # else len(ancestralRange) > 1
        if 'f' in eventTypes: # Full sympatry
            addOutcome(outcomes, (iR, iR), 'f')
            addComment(comments, (iR, iR), 'f', iR, ranges)

        if 'a' in eventTypes: # Allopatry: all possible pairs of non-empty subsets
            limitedSubRanges = True
            allopatricSubRanges = buildAllopatricSubRanges(ancestralRange, limitedSubRanges)

            for subRange in allopatricSubRanges:
                #print subRange
                # Get the left descendant range and ID
                leftChildRange = subRange[0]
                idLeftChildRange = -1
                if leftChildRange in mapRangesToId:
                    idLeftChildRange = mapRangesToId[leftChildRange]
                else:
                    continue
                # Get the right descendant range and ID
                rightChildRange = subRange[1]
                idRightChildRange = -1
                if rightChildRange in mapRangesToId:
                    idRightChildRange = mapRangesToId[rightChildRange]
                else:
                    continue

                # If both range exists
                addOutcome(outcomes, (idLeftChildRange, idRightChildRange), 'a')
                #print (idLeftChildRange, idRightChildRange)
                addComment(comments, (idLeftChildRange, idRightChildRange), 'a', iR, ranges)

        if 's' in eventTypes: # Subset sympatry: one keep the range, the other as only 1 area

            for area in ancestralRange:
                idArea = mapAreasToId[area]

                # left descendant has one area
                addOutcome(outcomes, (idArea, iR), 's')
                addComment(comments, (idArea, iR), 's', iR, ranges)

                # right descendant has one area
                addOutcome(outcomes, (iR, idArea), 's')
                addComment(comments, (iR, idArea), 's', iR, ranges)

        cladogeneticEventList.append(outcomes)
        cladogeneticCommentList.append(comments)

    normTerms = getPMatrixNormTerms(eventTypes, cladogeneticEventList)

    return [cladogeneticEventList, normTerms, cladogeneticCommentList]

def writeSingleRateCst(nEpoch, areas):

    strParams = '#Dispersal and extirpation rates\n'
    strParams += '#Dispersal rate\n'
    strParams += 'dispersal_rate <- 1.0\n'
    strParams += 'for (iEpoch in 1:' + repr(nEpoch) + '){\n'
    strParams += '  for (iA in 1:' + repr(len(areas)) + '){\n'
    strParams += '    for (jA in 1:' + repr(len(areas)) + '){\n'
    strParams += '        D[ iEpoch ][ iA ][ jA ] := 0.\n'
    strParams += '      if(iA!=jA){\n'
    strParams += '        D[ iEpoch ][ iA ][ jA ] := dispersal_rate\n'
    strParams += '} } } } \n\n'

    strParams += '#Extirpation rate - Setting a default prior and proposal\n'
    strParams += 'extirpation_rate <- 1.0\n'

    strParams += 'for (iEpoch in 1:' + repr(nEpoch) + '){\n'
    strParams += '  for (iA in 1:' + repr(len(areas)) + '){\n'
    strParams += '        E[ iEpoch ][ iA ] := extirpation_rate\n'
    strParams += '} } \n\n'

    return strParams

def writeSingleRate(nEpoch, areas):

    strParams = '#Dispersal and extirpation rates\n'
    strParams += '#Dispersal rate\n'
    strParams += 'dispersal_rate <- 1.0\n'
    strParams += 'for (iEpoch in 1:' + repr(nEpoch) + '){\n'
    strParams += '  for (iA in 1:' + repr(len(areas)) + '){\n'
    strParams += '    for (jA in 1:' + repr(len(areas)) + '){\n'
    strParams += '        D[ iEpoch ][ iA ][ jA ] := 0.\n'
    strParams += '      if(iA!=jA){\n'
    strParams += '        D[ iEpoch ][ iA ][ jA ] := dispersal_rate\n'
    strParams += '} } } } \n\n'

    strParams += '#Extirpation rate - Setting a default prior and proposal\n'
    strParams += 'log_sd <- 0.5 \n'
    strParams += 'log_mean <- ln(1) - 0.5*log_sd^2\n'
    strParams += 'extirpation_rate ~ dnLognormal(mean=log_mean, sd=log_sd)\n'
    strParams += 'moves[mvi++] = mvScale(extirpation_rate, weight=2)\n\n'

    strParams += 'for (iEpoch in 1:' + repr(nEpoch) + '){\n'
    strParams += '  for (iA in 1:' + repr(len(areas)) + '){\n'
    strParams += '        E[ iEpoch ][ iA ] := extirpation_rate\n'
    strParams += '} } \n\n'

    return strParams

def writeMultiRate(conMatrices, areas, coefficientsQ):
    listCoeff = coefficientsQ
    mapCoeff = {}
    for iC in range(len(listCoeff)):
        mapCoeff[listCoeff[iC]] = iC+1

    nEpoch = len(conMatrices)
    strParams = '#Dispersal and extirpation rates\n'
    strParams += '#Dispersal rates : '
    if len(listCoeff) > 0 : strParams += repr(listCoeff) + '\n'
    else: strParams += '\n'
    strParams += 'log_sd <- 0.5 \n'
    strParams += 'log_mean <- ln(1) - 0.5*log_sd^2\n'
    strParams += 'for (iDisp in 1:' + repr(len(coefficientsQ)) + '){\n'
    strParams += '  dispersal_rate[iDisp] ~ dnLognormal(mean=log_mean, sd=log_sd)\n'
    strParams += '  moves[mvi++] = mvScale(dispersal_rate[iDisp], weight=2)\n'
    strParams += '}\n\n'

    strParams += '#Extirpation rate - Setting a default prior and proposal\n'
    strParams += 'extirpation_rate <- 1.0\n'

    strParams += 'for (iEpoch in 1:' + repr(nEpoch) + '){\n'
    strParams += '  for (iA in 1:' + repr(len(areas)) + '){\n'
    strParams += '        E[ iEpoch ][ iA ] := extirpation_rate\n'
    strParams += '} } \n\n'

    for iE in range(len(conMatrices)):
        strParams += '# Epoch: ' + repr(iE+1) + '\n'
        for iA in range(1,len(conMatrices[iE])):
            for jA in range(1,len(conMatrices[iE][iA])):
                strParams += 'D[ ' + repr(iE+1) + ' ][ ' + repr(iA) + ' ][ ' + repr(jA) + ' ] := '
                if iA == jA:
                    strParams += '0.\n'
                elif isfloat(conMatrices[iE][iA][jA]):
                    strParams += repr(conMatrices[iE][iA][jA]) + '\n'
                else:
                    strParams += 'dispersal_rate[ ' + repr(mapCoeff[conMatrices[iE][iA][jA]]) + ' ] '
                    strParams += '# ' + conMatrices[iE][iA][jA] +  '\n'
    strParams += '\n'


    return strParams


def writeQMatrices(runType, qMatrices):

    strMatrices = ''

    if UGLY_MODE : # Bypass character limitation in RevBayes
        for iM in range(len(qMatrices)):
            for iR in range(len(qMatrices[iM])):
                for iC in range(len(qMatrices[iM][iR])):
                    strMatrices += 'rates[' + repr(iM+1) + '][' + repr(iR+1) + '][' + repr(iC+1) + '] := ' + qMatrices[iM][iR][iC] + ' \n'

            strMatrices += '\n'
    else:

        for iM in range(len(qMatrices)):
            strMatrices += 'rates[' + repr(iM+1) + '] := [ \n'
            for iR in range(len(qMatrices[iM])):
                strMatrices += '[ '
                for iC in range(len(qMatrices[iM][iR])):
                    strMatrices += qMatrices[iM][iR][iC]
                    if iC != len(qMatrices[iM][iR])-1:
                        strMatrices += ', '

                strMatrices += ' ]'
                if iR != len(qMatrices[iM])-1:
                    strMatrices += ','
                else:
                    strMatrices += ' ]'
                strMatrices += '\n'
            strMatrices += 'Q_DEC[' + repr(iM+1) + '] := fnFreeK( rates[' + repr(iM+1) + '] )'
            strMatrices += '\n\n'

    return strMatrices

def writeEpochs(epochs):
    strEpochs = 'time_bounds := ' + repr(epochs) + '\n'
    strEpochs += 'n_epochs := time_bounds.size()\n'
    strEpochs += '# build the time slices\n'
    strEpochs += 'for (i in 1:n_epochs) {\n'
    strEpochs += '  time_max[i] <- time_bounds[i][1]\n'
    strEpochs += '  time_min[i] <- time_bounds[i][2]\n'
    strEpochs += '  if (i == n_epochs) {\n'
    strEpochs += '    epoch_times[i] := 0.0\n'
    strEpochs += '  } else {\n'
    strEpochs += '    if (time_max[i] > time_min[i]) {\n'
    strEpochs += '       epoch_times[i] ~ dnUniform(time_min[i], time_max[i])\n'
    strEpochs += '       moves[mvi++] = mvSlide(epoch_times[i], delta=(time_max[i]-time_min[i])/2)\n'
    strEpochs += '    } else {\n'
    strEpochs += '       epoch_times[i] := time_max[i]\n'
    strEpochs += '} } }\n\n'

    return strEpochs

def writeCladogeneticEventProbs(runType, pMatrixType, nEpoch, eventTypes):
    strCladParams = '# Cladogenetic parameters [' + ', '.join(eventTypes) + '] \n'

    if pMatrixType == typeOfCladogeneticMatrix[0] or runType == typeOfRun[0]:
        strCladParams += 'pClad1 := simplex( [ ' + ','.join([repr(1.) for e in eventTypes]) + ' ] ) \n'
    elif pMatrixType == typeOfCladogeneticMatrix[1]:
        strCladParams += 'pClad1 ~ dnDirichlet( [ ' + ','.join([repr(1.) for e in eventTypes]) + ' ] ) \n'
        strCladParams += 'moves[mvi++] = mvSimplexElementScale(pClad1, alpha=10, weight=2) \n'
    elif pMatrixType == typeOfCladogeneticMatrix[2]:
        for i in range(1, nEpoch+1):
            strCladParams += 'pClad' + repr(i) + ' ~ dnDirichlet( [ ' + ','.join([repr(1.) for e in eventTypes]) + ' ] ) \n'
            strCladParams += 'moves[mvi++] = mvSimplexElementScale(pClad' + repr(i) + ' , alpha=10, weight=2) \n'

    return strCladParams


def writePMatrix(iMatrix, eventTypes, cladogeneticEventList, normTerms, cladogeneticCommentList):

    strPMatrix = '# pMatrix for epoch ' + repr(iMatrix+1) + '\n'

    if UGLY_MODE : # Bypass character limitation in RevBayes
        iOutcome = 1
        strNormTerms = ''
        strCladEvent = ''
        strCladEventProb = ''
        for iC in range(len(cladogeneticEventList)):

            # If we have zero or one event
            if normTerms[iC] == None:
                strNormTerms += 'normTerms[' + repr(iMatrix + 1)  + '][' + repr(iC+1) + '] := 0. \n'
                if len(cladogeneticEventList[iC]) == 1:
                    comment = cladogeneticCommentList[iC][(cladogeneticEventList[iC].keys()[0])]
                    strCladEvent += 'cladEvent[' + repr(iMatrix + 1)  + '][' + repr(iOutcome)  + '] := [' + repr(iC+1) + ',' + repr(iC+1) + ','  +repr(iC+1) + '] ' + comment + ' \n'
                    strCladEventProb += 'specRate[' + repr(iMatrix + 1)  + '][' + repr(iOutcome) + '] := 1. ' + comment + ' \n'
                    iOutcome += 1
                continue

            # else we have several events

            # prepare the normalization string
            terms = [ repr(factor) + '*pClad' + repr(iMatrix + 1) + '[' + repr(iE+1) + ']' for iE, eType, factor in zip(range(len(eventTypes)), eventTypes, normTerms[iC]) ]
            strNorm = '(' + '+'.join(terms) + ')'
            strNormTerms += 'normTerms[' + repr(iMatrix + 1)  + '][' + repr(iC+1) + '] := ' + strNorm + '\n'

            # deal with each event
            for event, types in cladogeneticEventList[iC].iteritems():

                comment = cladogeneticCommentList[iC][event]

                strCladEvent += 'cladEvent[' + repr(iMatrix + 1)  + '][' + repr(iOutcome)  + '] := [' + repr(iC+1) + ',' + repr(event[0]) + ','  +repr(event[1]) + '] ' + comment + ' \n'

                strProbs = [ 'pClad' + repr(iMatrix + 1) + '[' + repr(eventTypes.index(t)+1) + ']' for t in types]
                strCladEventProb += 'specRate[' + repr(iMatrix + 1)  + '][' + repr(iOutcome) + '] := ' + '+'.join(strProbs) + ' / normTerms[' + repr(iMatrix + 1)  + '][' + repr(iC+1) + '] ' + comment + ' \n'

                iOutcome += 1

        strCladEvent += '\n'
        strCladEventProb += '\n'
        strNormTerms += '\n'

        strPMatrix += strCladEvent + strNormTerms + strCladEventProb
    else:
        strNormTerms = 'normTerms[' + repr(iMatrix)  + '] := [ '
        strCladEvent = 'cladEvent [' + repr(iMatrix)  + ']:= [ '
        strCladEventProb = 'specRate[' + repr(iMatrix)  + '] := [ '
        for iC in range(len(cladogeneticEventList)):

            # If we have zero or one event
            if normTerms[iC] == None:
                if len(cladogeneticEventList[iC]) == 1:
                    strCladEvent += ' [' + repr(iC+1) + ',' + repr(iC+1) + ','  +repr(iC+1) + '], '
                    strCladEventProb += '1., '
                    strNormTerms += ' 0., '
                continue

            # else we have several events

            # prepare the normalization string
            strNorm = '('
            for iE, eType, factor in zip(range(len(eventTypes)), eventTypes, normTerms[iC]):
                strNorm += repr(factor) + '*p[' + repr(iMatrix + 1) + '][' + repr(iE+1) + ']'
            strNorm += ')'
            strNormTerms += strNorm + ', '

            # deal with each event
            for event, types in cladogeneticEventList[iC].iteritems():
                strCladEvent += ' [' + repr(iC+1) + ',' + repr(event[0]+1) + ','  +repr(event[1]+1) + '], '

                strProbs = [ 'p[' + repr(iMatrix + 1) + '][' + repr(eventTypes.index(t)+1) + ']' for t in types]
                strCladEventProb += '+'.join(strProbs) + ' / normTerms[' + repr(iMatrix + 1)  + '][' + repr(iC+1) + '], '

        # All events should be reported
        strCladEvent = strCladEvent[:-2] + ' ]\n\n' # erase unwanted comma, replace by ending ]
        strCladEventProb = strCladEventProb[:-2] + ' ]\n\n'
        strNormTerms = strNormTerms[:-2] + ' ]\n\n'

        strPMatrix += strCladEvent + strNormTerms + strCladEventProb

    return strPMatrix


def adaptDataFile(outputFolder, areas, ranges, dataFile):

    sep = ''
    if outputFolder[-1] != '/': sep = '/'
    modFile = outputFolder + sep + 'adaptedData.tsv'
    outFile = file(modFile, 'w')

    [mapAreasToId, mapBinToArea] = buildBinaryAreasMap(areas)
    mapRangesToId = buildRangesMap(ranges)

    n = NexusReader()
    n.read_file(dataFile)
    orderedKeys = n.data.matrix.keys()
    orderedKeys.sort()
    for taxon in orderedKeys:
        data = n.data.matrix[taxon]
        myRange = []
        offset=1

        for iA, c in zip(range(offset, len(areas)), data):
            if c == '1': myRange.append(areas[iA])

        state = mapRangesToId[tuple(myRange)]

        #n.data.matrix[taxon] = (repr(state))
        outFile.write(taxon + '\t' + repr(state) + '\n')

    relativeModFile = 'relPath' + ' + "/" + "' + modFile + '"'

    return relativeModFile


def writeInit(outputFolder, modDataFile, runType, areas, ranges, epochs, conMatrices, coefficientsQ, eventTypes):
    scriptContent = '#Proposal counter:\nmvi = 1\n\n'

    scriptContent += '# Read the modified data file (nex range states)\n'
    scriptContent += 'dat_range_n = readCharacterDataDelimited(' + modDataFile + ', stateLabels=(' + repr(len(ranges)) + '), type="NaturalNumbers", delimiter="\\t", headers=FALSE)\n\n'

    scriptContent += '#areas: '
    for a in areas: scriptContent += a + ', '
    scriptContent += '\n' + '#ranges: '
    for r in ranges: scriptContent += repr(r) + ', '
    scriptContent += '\n'
    scriptContent += 'nRanges := ' + repr(len(ranges)) + '\n\n'


    if runType == typeOfRun[2]:
        scriptContent += '#Epochs time slices\n'
        scriptContent += writeEpochs(epochs)

    if runType == typeOfRun[0]:
        scriptContent += writeSingleRateCst(len(conMatrices), areas)
    else:
        if len(coefficientsQ) == 0:
            scriptContent += writeSingleRate(len(conMatrices), areas)
        else:
            scriptContent += writeMultiRate(conMatrices, areas, coefficientsQ)

    scriptContent += writeCladogeneticEventProbs(runType, pMatrixType, len(epochs), eventTypes)

    scriptContent += '# Root frequencies\n'
    scriptContent += 'rf_DEC_tmp <- rep(1, nRanges)\n'
    scriptContent += 'rf_DEC_tmp[1] <- 0.\n'
    scriptContent += 'rf_DEC <- simplex(rf_DEC_tmp)\n\n'

    if runType == typeOfRun[0]:
        scriptContent += '# Branch rates\n'
        scriptContent += 'rate_bg <- 1\n\n'
    else:
        scriptContent += '# Branch rates\n'
        scriptContent += 'log10_rate_bg ~ dnUniform(-3,1)\n'
        scriptContent += 'log10_rate_bg.setValue(-2)\n'
        scriptContent += 'rate_bg := 10^log10_rate_bg\n'
        scriptContent += 'moves[mvi++] = mvSlide(log10_rate_bg, weight=4)\n\n'

    sep = ''
    if outputFolder[-1] != '/': sep = '/'
    oFile = open(outputFolder + sep + 'generated_Init.Rev', 'w')
    oFile.write(scriptContent)

def writeFileQMatrices(outputFolder, runType, areas, ranges, conMatrices, coefficientsQ, includeEmptyState):

    scriptContent = '# DEC anangenetic rates matrices (Q):\n'
    qMatrices = []
    for iM in range(len(conMatrices)):
        qMatrices.append(buildQMatrix(iM+1, areas, ranges, conMatrices[iM], coefficientsQ, includeEmptyState))

    scriptContent += writeQMatrices(runType, qMatrices)

    sep = ''
    if outputFolder[-1] != '/': sep = '/'
    oFile = open(outputFolder + sep + 'generated_QMatrices.Rev', 'w')
    oFile.write(scriptContent)

    return len(conMatrices)

def writeFileCladogeneticProbabilities(outputFolder, pMatrixType, areas, ranges, epochs, conMatrices, coefficientsQ, eventTypes):

    scriptContent = '# DEC cladogeneic rates matrices (P):\n'

    nPMatrix = 1
    if pMatrixType == typeOfCladogeneticMatrix[2]:
        nPMatrix = len(epochs)
        for iE in range(len(epochs)):
            [cladogeneticEventList, normTerms, cladogeneticCommentList] = buildCladogeneticEvents(eventTypes, areas, ranges, conMatrices[iE])
            scriptContent += writePMatrix(iE, eventTypes, cladogeneticEventList, normTerms, cladogeneticCommentList)
    else:
        [cladogeneticEventList, normTerms, cladogeneticCommentList] = buildCladogeneticEvents(eventTypes, areas, ranges, conMatrices[0])
        scriptContent += writePMatrix(0, eventTypes, cladogeneticEventList, normTerms, cladogeneticCommentList)

    sep = ''
    if outputFolder[-1] != '/': sep = '/'
    oFile = open(outputFolder + sep + 'generated_CladoProb.Rev', 'w')
    oFile.write(scriptContent)

    return nPMatrix

def writePhyloDistribution(outputFolder, runType, pMatrixType, nQMatrix, nPMatrix):

    scriptContent = '# Creation of the anagenetic transition matrix/matrices\n'
    for iM in range(nQMatrix):
        scriptContent += 'Q_DEC[' + repr(iM+1) + '] := fnFreeK( rates[' + repr(iM+1) + '], rescaled=false, matrixExponentialMethod="eigen")\n' #scalingAndSquaring

    if runType == typeOfRun[2]:
        scriptContent += 'Q_DEC_epoch := fnEpoch(Q=Q_DEC, times=epoch_times, rates=rep(1, n_epochs))\n\n'
    else:
        scriptContent += '\n'

    # Creating the pMatrix
    scriptContent += '# Creation of the cladogenetic transition matrix/matrices\n'
    for iM in range(nPMatrix):
        scriptContent += 'P_DEC[' + repr(iM+1) + '] := fnCladogeneticProbabilityMatrix(cladEvent[' + repr(iM + 1)  + '], specRate[' + repr(iM + 1)  + '], nRanges)\n\n'

    scriptContent += '# Biogeographic likelihood \n'
    scriptContent += 'myModel_BG ~ dnPhyloCTMCClado(tree=tree,\n'

    if runType == typeOfRun[2]:
        scriptContent += 'Q=Q_DEC_epoch,\n'
    else:
        scriptContent += 'Q=Q_DEC[1],\n'

    scriptContent += 'cladoProbs=P_DEC[1],\n'

    scriptContent += 'branchRates=rate_bg,\n'
    scriptContent += 'rootFrequencies=rf_DEC,\n'
    scriptContent += 'type="NaturalNumbers",\n'
    scriptContent += 'nSites=1)\n\n'

    sep = ''
    if outputFolder[-1] != '/': sep = '/'
    oFile = open(outputFolder + sep + 'generated_Model.Rev', 'w')
    oFile.write(scriptContent)

def writeRanges(outputFolder, ranges):
    sep = ''
    if outputFolder[-1] != '/': sep = '/'
    outFile = open(outputFolder + sep + 'state_label.txt', 'w')

    for iR, r in zip(range(len(ranges)), ranges):
        outFile.write(repr(iR)+'\t'+repr(r)+'\n')



# yes/no
includeEmptyState = True
# constant rates, single epoch, multiple epochs

# 'a': allopatry, 's': subset sympatry, 'f': full sympatry
eventTypes = ['a', 's']

idTypeOfRun = sys.argv[1]
idTypeOfCladogenMatrix = sys.argv[2]
rangesFile = sys.argv[3]
epochsFile = sys.argv[4]
connectivity_baseName = sys.argv[5]
dataFile = sys.argv[6]
outputFolder = sys.argv[7]

runType = typeOfRun[ int( idTypeOfRun )]
pMatrixType = typeOfCladogeneticMatrix[ int(idTypeOfCladogenMatrix) ]

# Read ranges
[areas, ranges] = readRanges(rangesFile)
# Read epochs
epochs = []
if runType == typeOfRun[2]:
    epochs = readEpochs(epochsFile)
# read connectivity matrices
[conMatrices, coefficientsQ] = [None, None]
if runType == typeOfRun[2]:
    [conMatrices, coefficientsQ] = readConnectivities(connectivity_baseName)
else:
    [conMatrices, coefficientsQ] = readConnectivity(connectivity_baseName)
# read Data file
modDataFile = adaptDataFile(outputFolder, areas, ranges, dataFile)

# Write init file
writeInit(outputFolder, modDataFile, runType, areas, ranges, epochs, conMatrices, coefficientsQ, eventTypes)
# Write Q matrices file
nQMatrix = writeFileQMatrices(outputFolder, runType, areas, ranges, conMatrices, coefficientsQ, includeEmptyState)

# Write P matrices file
nPMatrix = writeFileCladogeneticProbabilities(outputFolder, pMatrixType, areas, ranges, epochs, conMatrices, coefficientsQ, eventTypes)

writePhyloDistribution(outputFolder, runType, pMatrixType, nQMatrix, nPMatrix)

writeRanges(outputFolder, ranges)
